library(assertthat)
library(tidyverse)

split_numbers_by_empty_line <- function(lines) {
  numbers <- as.numeric(lines)
  empty_lines <- is.na(numbers)
  split_intervals <- cumsum(empty_lines)
  return(split(numbers[!empty_lines], split_intervals[!empty_lines]))
}

input_file <- file('day-1/input.txt')
elves <- split_numbers_by_empty_line(readLines(input_file))
close(input_file)

total_calories <- elves %>% map_dbl(~sum(.x))
max_calories <- max(total_calories)
assert_that(max_calories == 72602)
writeLines(str_interp(
  'Highest total calories: ${value}',
  list(value = max_calories)
))

top3_calories_sum <- sum(
  slice_max(tibble(total_calories), `total_calories`, n = 3)
)
assert_that(top3_calories_sum == 207410)
writeLines(str_interp(
  'Sum of top 3: ${value}',
  list(value = top3_calories_sum)
))
